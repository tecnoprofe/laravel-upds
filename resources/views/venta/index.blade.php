@extends('layouts.admin')
@section('title', 'ARTICULO')

@section('sidebar')
    @parent   
    ARTICULOS
@endsection
 
@section('content')
    <div class='jumbotrom'>                
        <a href="/venta/create" class="btn btn-primary">Crear</a>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Total</th>                    
                    <th scope="col">Opciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($ventas as $dato)                                    
            <tr>
                <th scope="row">{{$dato->id}}</th>
                <td>{{$dato->name}}</td>
                <td>{{$dato->monto_total}}</td>                                
                <td>
                    <a href="{{ route('venta.edit', $dato->id) }} " class="btn btn-primary">Editar</a>
                    <form action="/venta/{{$dato->id}}" method="post">
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                        @csrf
                    </form>

                    <a href="{{ route('pdf1', $dato->id) }}" class="btn btn-warning">PDF Venta</a>
                </td>
            </tr>                
            @endforeach        
            </tbody>
        </table>
        </ul>
    </div> 
@endsection