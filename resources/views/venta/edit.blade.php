@extends('layouts.admin')
@section('title', 'Crear Articulo')

@section('sidebar')
    @parent   
    Crear Articulo
@endsection
 
@section('content')
    <div class='jumbotrom'>

        <form method="POST" action="{{ route('articulo.update', $articulo->id) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="_method" value="PUT" />
                    
            <div class="mb-3">
                <label class="form-label">Nombre</label>                
                <input type="text" name="nombre" class="form-control" value="{{ $articulo->nombre}}"  placeholder="Nombre"/>

            </div>                   
            <div class="mb-3">
                <label class="form-label">Color</label>
                <input type="text" name="color" class="form-control" value="{{ $articulo->color}}"  placeholder="Color"/>                
            </div>                   
            <div class="mb-3">
                <label class="form-label">Precio</label>
                <input type="text" name="precio" class="form-control" value="{{ $articulo->precio}}"  
                placeholder="Precio"/>                                
            </div>                   
            <div class="mb-3">
                <label class="form-label">Stock</label>
                <input type="text" name="stock" class="form-control" value="{{ $articulo->stock}}"  placeholder="Stock"/>                
            </div>                   
            <div class="mb-3">
                <label class="form-label">Descripcion</label>
                
                <textarea class="form-control" rows="3" name="descripcion" value="{{ $articulo->descripcion}}"></textarea>
            </div>                   
            <div class="mb-3">
                <input type="submit" class="btn btn-secondary" value="Actualizar">
            </div>                   
        </form>
        
 
    </div> 
@endsection
    