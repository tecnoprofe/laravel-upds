@extends('layouts.admin')
@section('title', 'Crear Articulo')

@section('sidebar')
    @parent   
    Crear Articulo
@endsection
 
@section('content')
    <div class='jumbotrom'>
        <form method="POST" action="/articulo" >
            @csrf
            <div class="mb-3">
                <label class="form-label">Nombre</label>
                <input type="text" name='nombre' class="form-control" placeholder="Nombre">
            </div>                   
            <div class="mb-3">
                <label class="form-label">Color</label>
                <input type="text" name='color' class="form-control" placeholder="Color">
            </div>                   
            <div class="mb-3">
                <label class="form-label">Precio</label>
                <input type="text" name='precio' class="form-control" placeholder="Precio">
            </div>                   
            <div class="mb-3">
                <label class="form-label">Stock</label>
                <input type="text" name='stock' class="form-control" placeholder="Stock">
            </div>                   
            <div class="mb-3">
                <label class="form-label">Descripcion</label>
                <textarea class="form-control" name='descripcion' rows="3"></textarea>            
            </div>                   
            <div class="mb-3">
                <input type="submit" class="btn btn-primary" value="Guardar">
            </div>                   
        </form>
    </div> 
@endsection