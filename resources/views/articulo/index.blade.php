@extends('layouts.admin')
@section('title', 'ARTICULO')

@section('sidebar')
    @parent   
    ARTICULOS
@endsection
 
@section('content')
    <div class='jumbotrom'>
        
        
        <a href="/articulo/create" class="btn btn-primary">Crear</a>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Color</th>
                    <th scope="col">Stock</th>
                    <th scope="col">Descripcion</th>
                    <th scope="col">Opciones</th>
                </tr>
            </thead>
            <tbody>
        @foreach($articulos as $articulo)                                    
            <tr>
                <th scope="row">{{$articulo->id}}</th>
                <td>{{$articulo->nombre}}</td>
                <td>{{$articulo->precio}}</td>
                <td>{{$articulo->color}}</td>
                <td>{{$articulo->stock}}</td>
                <td>{{$articulo->descripcion}}</td>
                <td>
                    
                    <a href="{{ route('articulo.edit', $articulo->id) }} " class="btn btn-primary">Editar</a>                    

                    <form action="/articulo/{{$articulo->id}}" method="post">
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                        @csrf
                    </form>
                    
                
                </td>
            </tr>                
        @endforeach        
            </tbody>
        </table>

        </ul>
    </div> 
@endsection