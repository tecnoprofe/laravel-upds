<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticuloController;
use App\Http\Controllers\VentaController;

use App\Models\Venta;
use App\Models\User;
use App\Models\Articulo;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/pdf1/{id}', function ($id) {       
    $venta= Venta::find($id);    
    $detalles=$venta->articulos;
    
    $pdf = PDF::loadView('venta.pdf1',compact('venta','detalles'));
    return $pdf->stream('invoice.pdf');
    
})->name('pdf1');


Route::get('/', function () {
    return view('layouts.admin');
});

Route::resource('articulo',ArticuloController::Class);
Route::resource('venta',VentaController::Class);